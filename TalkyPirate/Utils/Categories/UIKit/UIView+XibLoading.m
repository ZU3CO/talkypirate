//
//  UIView+XibLoading.m
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "UIView+XibLoading.h"

@implementation UIView (XibLoading)

- (void)tp_loadViewFromNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:bundle];
    
    [nib instantiateWithOwner:self options:nil];
}

@end
