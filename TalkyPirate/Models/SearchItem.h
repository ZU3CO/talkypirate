//
//  SearchItem.h
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@interface SearchItem : MTLModel <MTLJSONSerializing>

@property (readonly, nonatomic, strong) NSString *english;
@property (readonly, nonatomic, strong) NSString *pirate;

@end
