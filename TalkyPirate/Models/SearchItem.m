//
//  SearchItem.m
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "SearchItem.h"

@implementation SearchItem

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"english": @"translation.english",
             @"pirate": @"translation.pirate"
             };
}

@end
