//
//  APIClient.m
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "APIClient.h"
#import <AFNetworking/AFHTTPSessionManager.h>


static NSString * const kBaseUrlString = @"http://isithackday.com/";

@interface APIClient ()

@property (readwrite, nonatomic) NSURL *baseURL;
@property (nonatomic, strong) AFHTTPSessionManager *sessionManger;

@end


@implementation APIClient

- (instancetype)init {
    if (self = [super init]) {
        NSURL *url = [NSURL URLWithString:kBaseUrlString];
        self.baseURL = url;
        self.sessionManger = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        self.sessionManger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", nil];
        self.sessionManger.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)GET:(NSString *)URLString
 parameters:(NSDictionary *)parameters
    success:(void (^)(id _Nullable result))success
    failure:(void (^)(NSError * _Nonnull error))failure {
    
    [self.sessionManger GET:URLString
                 parameters:parameters
                   progress:nil
                    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        if (success) {
                            success(responseObject);
                        }
                    }
                    failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        if (failure) {
                            failure(error);
                        }
                    }];
}

- (void)POST:(NSString *)URLString
  parameters:(NSDictionary *)parameters
     success:(void (^)(id _Nullable))success
     failure:(void (^)(NSError * _Nonnull))failure {
    
    [self.sessionManger POST:URLString
                  parameters:parameters
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         if (success) {
                             success(responseObject);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure(error);
                         }
                     }];
}

@end
