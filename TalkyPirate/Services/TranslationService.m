//
//  TranslationService.m
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "TranslationService.h"
#import "SearchItem.h"

static NSString * const kTranslationPath = @"arrpi.php";

@implementation TranslationService

- (instancetype)initWithClient:(APIClient *)client {
    if (self = [super init]) {
        self.client = client;
    }
    
    return self;
}

- (void)getTranslationFor:(NSString *)originalText
                  success:(nullable SearchItem_block_t)success
                  failure:(nullable NSError_block_t)failure {
    
    NSDictionary *parameters = @{@"text" : originalText,
                                 @"format" : @"json"
                                 };
    
    [self.client GET:kTranslationPath
          parameters:parameters
             success:^(id  _Nullable result) {
                 if (success && result) {
                     NSError *error = nil;
                     SearchItem *user = [MTLJSONAdapter modelOfClass:SearchItem.class
                                                  fromJSONDictionary:result
                                                               error:&error];
                     if (user) {
                         success(user);
                     } else if (failure) {
                         failure(error);
                     }
                 }
             }
             failure:^(NSError * _Nonnull error) {
                 if (failure) {
                     failure(error);
                 }
             }];
}

@end
