//
//  APIClient.h
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APIClient : NSObject

/**
 The URL used to construct requests from relative paths.
 */
@property (readonly, nonatomic) NSURL *baseURL;

/**
 Performs a `GET` request.
 
 @param URLString The URL string used to create the request URL.
 @param parameters The parameters to be encoded according to the client request serializer.
 @param success A block object to be executed when the task finishes successfully.
 @param failure A block object to be executed when the task finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 */
- (void)GET:(NSString *)URLString
 parameters:(nullable NSDictionary *)parameters
    success:(nullable void (^)(id _Nullable result))success
    failure:(nullable void (^)(NSError *error))failure;

/**
 Performs a `POST` request.
 
 @param URLString The URL string used to create the request URL.
 @param parameters The parameters to be encoded according to the client request serializer.
 @param success A block object to be executed when the task finishes successfully.
 @param failure A block object to be executed when the task finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 */
- (void)POST:(NSString *)URLString
  parameters:(nullable NSDictionary *)parameters
     success:(nullable void (^)(id _Nullable result))success
     failure:(nullable void (^)(NSError *error))failure;

NS_ASSUME_NONNULL_END

@end
