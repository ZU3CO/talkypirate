//
//  TranslationService.h
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClient.h"

@class SearchItem;

NS_ASSUME_NONNULL_BEGIN

typedef void(^NSError_block_t)(NSError *);
typedef void(^SearchItem_block_t)(SearchItem *);

@interface TranslationService : NSObject

/**
 The `HTTP client` used to perform `GET`, `POST`, .. requests.
 */
@property (nonatomic, strong) APIClient *client;

/**
 Initializes an `TranslationService` object with the specified HTTP client.
 
 @param client The HTTP client.
 
 @return The newly-initialized TranslationService
 */
- (instancetype)initWithClient:(APIClient *)client;

/**
 Performs a `GET` request, in order to get a translation for a specified string.
 
 @param originalText The string to be translated.
 @param success A block object to be executed when the request finishes successfully.
 @param failure A block object to be executed when the request finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data into model.
 */
- (void)getTranslationFor:(NSString *)originalText
                  success:(nullable SearchItem_block_t)success
                  failure:(nullable NSError_block_t)failure;

NS_ASSUME_NONNULL_END

@end
