//
//  TranslationInputView.h
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingButton.h"

@interface TranslationInputView : UIView

/// If you want to customize this view by simply adding additional views, you should add them to the content view
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UITextView *translationTextView;
@property (weak, nonatomic) IBOutlet LoadingButton *actionButtton;

@end
