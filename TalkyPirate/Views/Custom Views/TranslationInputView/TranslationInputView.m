//
//  TranslationInputView.m
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "TranslationInputView.h"
#import "UIView+XibLoading.h"

@implementation TranslationInputView

#pragma mark - Lifecycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

/**
 Called at init time.
 
 Call [super commonInit] when use this method at inheritance.
 */
- (void)commonInit {
    [self tp_loadViewFromNib];
    
    [self.contentView setFrame:self.bounds];
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:self.contentView];
}

#pragma mark - Custom Accessors


#pragma mark - IBActions


#pragma mark - Public


#pragma mark - Private


@end
