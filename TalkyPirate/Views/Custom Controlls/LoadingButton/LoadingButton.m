//
//  LoadingButton.m
//  TalkyPirate
//
//  Created by Pavel on 5/9/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "LoadingButton.h"

@interface LoadingButton ()

@property(nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation LoadingButton

#pragma mark - Lifecycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    [self setupActivityIndicator];
}

- (void)setupActivityIndicator {
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.frame = self.bounds;
    self.activityIndicator.hidesWhenStopped = YES;
    [self.activityIndicator stopAnimating];
    
    [self addSubview:_activityIndicator];
}

- (void)setLoading:(BOOL)loading {
    @synchronized(self){
        _loading = loading;
        self.enabled = !self.loading;
        if (self.loading){
            self.titleLabel.layer.opacity = 0.f;
            [_activityIndicator startAnimating];
        } else {
            self.titleLabel.layer.opacity = 1.f;
            [_activityIndicator stopAnimating];
        }
    }
}

@end
