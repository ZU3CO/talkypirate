//
//  LoadingButton.h
//  TalkyPirate
//
//  Created by Pavel on 5/9/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingButton : UIButton

/*
  Hidding/unhidding titleLabel and starting/stoping the UIActivityIndicatorView animation. 
 */
@property (nonatomic, assign) BOOL loading;

@end
