//
//  ViewController.m
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import "ViewController.h"
#import "TranslationInputView.h"
#import "APIClient.h"
#import "TranslationService.h"
#import "SearchItem.h"

static NSString * const kEnglishHeaderTitle = @"ENGLISH";
static NSString * const kPirateHeaderTitle = @"PIRATE";
static NSString * const kTranslateButtonTitle = @"ARR!!";

@interface ViewController ()

@property (weak, nonatomic) IBOutlet TranslationInputView *inputView;
@property (weak, nonatomic) IBOutlet TranslationInputView *outputView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}

- (void)setupSubviews {
    // Setup inputView
    self.inputView.languageLabel.text = kEnglishHeaderTitle;
    [self.inputView.actionButtton setTitle:kTranslateButtonTitle
                                  forState:UIControlStateNormal];
    // Setup outputView
    self.outputView.languageLabel.text = kPirateHeaderTitle;
    self.outputView.actionButtton.hidden = YES;
    self.outputView.translationTextView.editable = NO;
    self.outputView.translationTextView.text = @"";
}

- (TranslationService *)translationService {
    if (!_translationService) {
        APIClient *client = [[APIClient alloc] init];
        _translationService = [[TranslationService alloc] initWithClient:client];
    }
    
    return _translationService;
}

- (IBAction)translateAction:(id)sender {
    
    // Start button - activityIndicator animation and hide the keyboard
    self.inputView.actionButtton.loading = YES;
    [self.inputView.translationTextView resignFirstResponder];
    
    NSString *originalText = self.inputView.translationTextView.text;
    __weak typeof (self) weakSelf = self;
    
    [self.translationService getTranslationFor:originalText
                                       success:^(SearchItem * _Nonnull searchItem) {
                                           // Stop button - activityIndicator animation and show the translation result
                                           weakSelf.inputView.actionButtton.loading = NO;
                                           weakSelf.outputView.translationTextView.text = searchItem.pirate;
                                       }
                                       failure:^(NSError * _Nonnull error) {
                                           // Stop button - activityIndicator animation
                                           weakSelf.inputView.actionButtton.loading = NO;
                                           weakSelf.outputView.translationTextView.text = error.localizedDescription;
                                       }];
}

@end
