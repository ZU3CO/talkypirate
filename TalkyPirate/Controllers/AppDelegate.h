//
//  AppDelegate.h
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

