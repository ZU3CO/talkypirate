//
//  ViewController.h
//  TalkyPirate
//
//  Created by Pavel on 5/8/16.
//  Copyright © 2016 ZUBCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TranslationService;

@interface ViewController : UIViewController

@property (nonatomic, strong) TranslationService *translationService;

@end

