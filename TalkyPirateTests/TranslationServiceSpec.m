//
//  TranslationServiceSpec.m
//  TalkyPirate
//
//  Created by Pavel on 5/9/16.
//  Copyright 2016 ZUBCO. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "TranslationService.h"
#import "SearchItem.h"

SPEC_BEGIN(TranslationServiceSpec)

describe(@"TranslationService", ^{
    
    __block APIClient *clientMock = nil;
    __block TranslationService *translationService = nil;
    
    beforeEach(^{
        clientMock = [KWMock mockForClass:[APIClient class]];
        translationService = [[TranslationService alloc] initWithClient:clientMock];
    });
    
    afterEach(^{
        clientMock = nil;
        translationService = nil;
    });
    
    it(@"Should return an non nil object", ^{
        [clientMock stub:@selector(GET:parameters:success:failure:) withBlock:^id(NSArray *params) {
            void (^successBlock)(id) = params[2];
            
            NSString *dummyString = @"{\"translation\":{\"english\":\"hello\",\"pirate\":\"avast\"}}";
            NSData *stringData = [dummyString dataUsingEncoding:NSUTF8StringEncoding];
            
            // Dummy JSON
            NSError *error;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:stringData options:0 error:&error];
            
            // Call back by executing the completion block parameter
            successBlock(jsonDict);
            
            return nil;
        }];
        
        __block NSString *translation = nil;
        [translationService getTranslationFor:@"Hello"
                                      success:^(SearchItem * _Nonnull search) {
                                          translation = search.english;
                                      }
                                      failure:^(NSError * _Nonnull error) {
                                      }];
        
        [[expectFutureValue(translation) shouldEventually] beNonNil];
    });
    
    it(@"Should return an error", ^{
        [clientMock stub:@selector(GET:parameters:success:failure:) withBlock:^id(NSArray *params) {
            void (^failureBlock)(id) = params[3];
            
            // Call back by executing the completion block parameter
            NSError *dummyError = [[NSError alloc] init];
            failureBlock(dummyError);
            
            return nil;
        }];
        
        __block NSError *futureError = nil;
        [translationService getTranslationFor:@"Hello"
                                      success:^(SearchItem * _Nonnull search) {
                                          
                                      }
                                      failure:^(NSError * _Nonnull error) {
                                          futureError = error;
                                      }];
        
        [[expectFutureValue(futureError) shouldEventually] beNonNil];
    });
});

SPEC_END
